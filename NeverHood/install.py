import _winreg
import os
def set_reg(name, value,type):
    _winreg.CreateKey(_winreg.HKEY_LOCAL_MACHINE, REG_PATH)
    registry_key = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, REG_PATH, 0, 
                                   _winreg.KEY_WRITE)
    if type=="string":
        t=_winreg.REG_SZ
    else:
        t=_winreg.REG_DWORD
    _winreg.SetValueEx(registry_key, name, 0, t, value)
    _winreg.CloseKey(registry_key)
    return True


filepath= os.path.abspath(os.path.join(os.getcwd(), '..'))
if 'PROGRAMFILES(X86)' in os.environ:
	REG_PATH = r"SOFTWARE\WOW6432Node\DreamWorks Interactive\Neverhood"
	_winreg.CreateKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\WOW6432Node\DreamWorks Interactive")
else:
	REG_PATH = r"SOFTWARE\DreamWorks Interactive\Neverhood"
	_winreg.CreateKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\DreamWorks Interactive")

set_reg("Data Drive",filepath,"string")
set_reg("Installed",0x00000001,"dword")
set_reg("Installed Directory",filepath,"string")
set_reg("KBPS",0xFFFFFFFF ,"dword")
set_reg("KBPS_OTHER",0xFFFFFFFF ,"dword")
set_reg("PID","54767-442-8398851-43971" ,"string")
set_reg("Run File",os.path.join(filepath,"NHC") ,"string")
