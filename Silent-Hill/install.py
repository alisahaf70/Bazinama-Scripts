import _winreg
import os
def set_reg(name, value,type):
    _winreg.CreateKey(_winreg.HKEY_LOCAL_MACHINE, REG_PATH)
    registry_key = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, REG_PATH, 0, 
                                   _winreg.KEY_WRITE)
    if type=="string":
        t=_winreg.REG_SZ
    else:
        t=_winreg.REG_DWORD
    _winreg.SetValueEx(registry_key, name, 0, t, value)
    _winreg.CloseKey(registry_key)
    return True
def set_reguser(name, value,type):
    _winreg.CreateKey(_winreg.HKEY_CURRENT_USER, REG_PATH)
    registry_key = _winreg.OpenKey(_winreg.HKEY_CURRENT_USER, REG_PATH, 0, 
                                   _winreg.KEY_WRITE)
    if type=="string":
        t=_winreg.REG_SZ
    else:
        t=_winreg.REG_DWORD
    _winreg.SetValueEx(registry_key, name, 0, t, value)
    _winreg.CloseKey(registry_key)
    return True

filepath= os.path.abspath(os.path.join(os.getcwd(), '..'))
if 'PROGRAMFILES(X86)' in os.environ:
	REG_PATH = r"SOFTWARE\WOW6432Node\Silent Hill"
else:
	REG_PATH = r"SOFTWARE\Silent Hill"

set_reg("ConfigRanOnce","YES","string")
set_reg("DeleteGPUSettings","NO","string")
set_reg("Path",filepath ,"string")

_winreg.CreateKey(_winreg.HKEY_CURRENT_USER, "SOFTWARE\epsxe")
REG_PATH=r"SOFTWARE\epsxe\config"
set_reguser("AutoPpfLoad","1","string")
set_reguser("BiosName",".\\SCPH1001.BIN","string")
set_reguser("CdromLetter","0","string")
set_reguser("CdromPlugin","W2KCDRCORE","string")
set_reguser("Country","0","string")
set_reguser("GamepadAxis","0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0","string")
set_reguser("GamepadBMotorType","0,0,0,0","string")
set_reguser("GamepadMotorType","0,0,0,0","string")
set_reguser("GamepadSMotorType","0,0,0,0","string")
set_reguser("GamepadSubType","0,0,0,0","string")
set_reguser("GamepadType","6,0,1,1","string")
set_reguser("IsoDirectory",".\\data.iso","string")
set_reguser("Keys1","203,205,200,208,14,15,57,42,16,44,18,29,25,23,44,45","string")
set_reguser("Keys2","0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0","string")
set_reguser("Keys3","0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0","string")
set_reguser("Keys4","0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0","string")
set_reguser("Logswindow","0","string")
set_reguser("Memcard1",".\\saves.mcr","string")
set_reguser("Memcard2",".\\saves2.mcr","string")
set_reguser("Multitap1","0","string")
set_reguser("NetPlugin","DISABLED","string")
set_reguser("SoundCDDA","1","string")
set_reguser("SoundEnabled","1","string")
set_reguser("SoundPlugin","SPUCORE","string")
set_reguser("SoundSpuIrq","0","string")
set_reguser("SoundXA","1","string")
set_reguser("SubchannelW2kCaching","0","string")
set_reguser("SubchannelW2kCachingLG","0","string")
set_reguser("SubchannelW2kCdromEnabled","1","string")
set_reguser("SubchannelW9xCaching","0","string")
set_reguser("SubchannelW9xCachingLG","0","string")
set_reguser("SubchannelW9xCdromEnabled","1","string")
set_reguser("Version","67072","string")
set_reguser("VideoPlugin","gpuPeteD3D.dll","string")

_winreg.CreateKey(_winreg.HKEY_CURRENT_USER, "SOFTWARE\Vision Thing")
_winreg.CreateKey(_winreg.HKEY_CURRENT_USER, "SOFTWARE\Vision Thing\PSEmu Pro")
_winreg.CreateKey(_winreg.HKEY_CURRENT_USER, "SOFTWARE\Vision Thing\PSEmu Pro\GPU")
REG_PATH=r"SOFTWARE\Vision Thing\PSEmu Pro\GPU\PeteD3D"
set_reguser("ColDepth",0x00000020,"dword")
set_reguser("DrawDither",0x00000001,"dword")
set_reguser("FastMdec",0x00000001,"dword")
set_reguser("FilterType",0x00000006,"dword")
set_reguser("FrameLimit",0x00000002,"dword")
set_reguser("FrameReadType",0x00000004,"dword")
set_reguser("FrameTexType",0x00000002,"dword")
set_reguser("FullscreenBlur",0x00000001,"dword")
set_reguser("HiResTextures",0x00000001,"dword")
set_reguser("NoScreenSaver",0x00000001,"dword")
set_reguser("OffscreenDrawing",0x00000001,"dword")
set_reguser("OffscreenDrawingEx",0x00000003,"dword")
set_reguser("OpaquePass",0x00000001,"dword")
set_reguser("ResX",0x00000500,"dword")
set_reguser("ResY",0x00000400,"dword")
set_reguser("TexQuality",0x00000003,"dword")
set_reguser("UseFixes",0x00000000,"dword")
set_reguser("UseFrameLimit",0x00000001,"dword")
set_reguser("UseFrameSkip",0x00000001,"dword")
set_reguser("UseMask",0x00000001,"dword")
set_reguser("UseMultiPass",0x00000002,"dword")

