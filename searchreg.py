import _winreg
def traverse(root, key, list):
    hKey = _winreg.OpenKey(root, key)
    try:
        i=0
        while True:
            strFullSubkey=""
            try:
                strSubkey=_winreg.EnumKey(hKey,i)
                if (key!=""):
                    strFullSubkey=key+"\\"+strSubkey
                else:
                    strFullSubkey=strSubkey
            except WindowsError:
                hKey.Close()
                return
            traverse(root,strFullSubkey,list)
            list.append(strFullSubkey)
            i+=1
    except WindowsError:
        hKey.Close()
global list
list=list()
traverse(_winreg.HKEY_CURRENT_USER ,"",list)
print(list)