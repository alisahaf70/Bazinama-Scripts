import os, winshell
from win32com.client import Dispatch

desktop = winshell.desktop()
path  =os.getcwd()+"\\..\\Counter-Strike.lnk"
target = os.getcwd()+'\\..\\hl.exe'
wDir = os.getcwd()
wDir=wDir[:-7]
icon = os.getcwd()+"\..\cstrike\cstrike.ico"
arguments=r" -steam -game cstrike"
shell = Dispatch('WScript.Shell')
shortcut = shell.CreateShortCut(path)
shortcut.Targetpath = target
shortcut.arguments=arguments
shortcut.WorkingDirectory = wDir
shortcut.IconLocation = icon
shortcut.save()