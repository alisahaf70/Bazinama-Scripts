import os, winshell
from win32com.client import Dispatch

desktop = winshell.desktop()
path  =os.getcwd()+"\\..\\Disney's Aladdin.lnk"
target = os.getcwd()+'\\..\\DOSBOX\\DOSBox.exe'
wDir = os.getcwd()+"\..\DOSBOX"
icon = os.getcwd()+"\..\icon.ico"
arguments=r" -conf ..\dosbox_aladdin.conf -conf ..\dosbox_aladdin_single.conf -noconsole -c exit"
shell = Dispatch('WScript.Shell')
shortcut = shell.CreateShortCut(path)
shortcut.Targetpath = target
shortcut.arguments=arguments
shortcut.WorkingDirectory = wDir
shortcut.IconLocation = icon
shortcut.save()