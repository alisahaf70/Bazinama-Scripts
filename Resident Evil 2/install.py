import _winreg
import os
def set_reg(name, value,type):
    _winreg.CreateKey(_winreg.HKEY_LOCAL_MACHINE, REG_PATH)
    registry_key = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, REG_PATH, 0, 
                                   _winreg.KEY_WRITE)
    if type=="string":
        t=_winreg.REG_SZ
    else:
        t=_winreg.REG_DWORD
    _winreg.SetValueEx(registry_key, name, 0, t, value)
    _winreg.CloseKey(registry_key)
    return True


filepath= os.path.abspath(os.path.join(os.getcwd(), '..'))
if 'PROGRAMFILES(X86)' in os.environ:
	REG_PATH = r"SOFTWARE\WOW6432Node\CAPCOM\BIOHAZARD 2 PC"
	_winreg.CreateKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\WOW6432Node\CAPCOM")
else:
	REG_PATH = r"SOFTWARE\CAPCOM\BIOHAZARD 2 PC"
	_winreg.CreateKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\CAPCOM")

set_reg("ExecuteProgram",os.path.join(filepath, 'bio2.exe'),"string")
set_reg("InstallKey","BH82120691132478","string")
set_reg("SavePath",os.path.join(filepath, 'savedata'),"string")
