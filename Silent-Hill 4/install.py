import _winreg
import os
def set_reg(name, value,type):
    _winreg.CreateKey(_winreg.HKEY_LOCAL_MACHINE, REG_PATH)
    registry_key = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, REG_PATH, 0, 
                                   _winreg.KEY_WRITE)
    if type=="string":
        t=_winreg.REG_SZ
    else:
        t=_winreg.REG_DWORD
    _winreg.SetValueEx(registry_key, name, 0, t, value)
    _winreg.CloseKey(registry_key)
    return True


filepath= os.path.abspath(os.path.join(os.getcwd(), '..'))
if 'PROGRAMFILES(X86)' in os.environ:
	REG_PATH = r"SOFTWARE\WOW6432Node\Konami\SILENT HILL 4\1.00.000"
	_winreg.CreateKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\WOW6432Node\Konami")
	_winreg.CreateKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\WOW6432Node\Konami\SILENT HILL 4")
else:
	REG_PATH = r"SOFTWARE\Konami\SILENT HILL 4\1.00.000"
	_winreg.CreateKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\Konami")
	_winreg.CreateKey(_winreg.HKEY_LOCAL_MACHINE, "SOFTWARE\Konami\SILENT HILL 4")

set_reg("Install Language","English","string")
set_reg("Install Path",filepath,"string")
set_reg("Movie Install",filepath ,"string")
set_reg("Uninstall Path","" ,"string")

