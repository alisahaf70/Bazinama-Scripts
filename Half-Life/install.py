import _winreg
def set_reg(name, value,type):
    _winreg.CreateKey(_winreg.HKEY_CURRENT_USER, REG_PATH)
    registry_key = _winreg.OpenKey(_winreg.HKEY_CURRENT_USER, REG_PATH, 0, 
                                   _winreg.KEY_WRITE)
    if type=="string":
        t=_winreg.REG_SZ
    else:
        t=_winreg.REG_DWORD
    _winreg.SetValueEx(registry_key, name, 0, t, value)
    _winreg.CloseKey(registry_key)
    return True


REG_PATH = r"SOFTWARE\Valve\Half-Life\Settings"
_winreg.CreateKey(_winreg.HKEY_CURRENT_USER, "SOFTWARE\Valve")
_winreg.CreateKey(_winreg.HKEY_CURRENT_USER, "SOFTWARE\Valve\Half-Life")


set_reg("Key","2335402628334","string")
